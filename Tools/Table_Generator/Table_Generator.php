<?php
namespace Tools\Table_Generator;

/**
* @author Ammar F. <ammarfaizi2@gmail.com> https://www.facebook.com/ammarfaizi2
* @license RedAngel_PHP_Concept (c) 2017
* @package Table Generator
*/
class Table_Generator {
  const LICENSE = "<strong>RedAngel_PHP_Concept (c) 2017</strong>";
  public function __construct($datatable,$color='yellow',$op=null){
    $this->datatable = $datatable;
    $this->color     = $color;
    $this->op        = $op===null ? ' border="1" align="center" style="border-collapse:collapse;background-color:'.$this->color.'" id="q"' : $op;
  }
  public function table_name($a){
    if (!is_array($a)||count($a)!==2) {
      throw new Exception("Table name must be array and has two index keys!\n\n\n".self::LICENSE,1);
    }
    $this->tablename = $a;
  }
  public function generate(){
    if (!isset($this->tablename)) {
      $this->tablename = array('Group','Single');
    }
    $html = "";
    $g = 0;
    for ($i=0;$i<count($this->datatable);$i++) { 
      if ($i===0) {
        $a['t'.$g][] = $this->datatable[$i];
      } else
      if ($this->datatable[$i]['is-group']===true) {
        $a['t'.$g][] = $this->datatable[$i];
      } else {
        $a['t'.(++$g)][] = $this->datatable[$i];
      }
    }
    for ($i=0;$i<count($a);$i++) { 
      $cols = count($a['t'.$i]);
      $rows = count($a['t'.$i][0]['data']);
      $html.= '<table '.$this->op.'><tr><td align="center" colspan="'.$cols.'">'.($a['t'.$i][0]['is-group']===true?$this->tablename[0]:$this->tablename[1]).'</td></tr>';
      for ($j=0;$j<$rows;$j++) { 
        $html.= '<tr>';
        for ($k=0;$k<$cols;$k++) { 
          $html.= '<td>'.$a['t'.$i][$k]['data'][$j].'</td>';
        }
        $html.= '<tr>';
      }
    }
    return $html;
  }
}