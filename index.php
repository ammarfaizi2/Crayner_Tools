<?php
require_once('./Tools/Table_Generator/Table_Generator.php');
use Tools\Table_Generator\Table_Generator;
$data = array(
  'matematika',
  'ips',
  'ipa',
  'bahasa indonesia',
  'bahasa inggris'
);
$datatable = array(
  array(
    'title' => 'table 1',
    'is-group' => true,
    'data' => $data
  ),
  array(
    'title' => 'table 2',
    'is-group' => true,
    'data' => $data
  ),
  array(
    'title' => 'table 3',
    'is-group' => false,
    'data' => $data
  ),
);
$namaTable = array(
    'Group',
    'Single'
);
?>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
  #q{
    display:inline-block;
    margin:10px;
  }
  td{
    padding:10px 30px 10px 30px;
    align:center;
    text-align: center;
  }
</style>
  <title>Table Generator</title>
</head>
<body>
<?php
$a = new Table_Generator($datatable,'yellow');
echo $a->generate();
?>